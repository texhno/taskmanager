% FLASKAPP(1) taskmanager 1.0.0
% Decentrala
% Jun 2023

# NAME
taskmanager - Web app

# SYNOPSIS
**python3 run.py**

# DESCRIPTION 
Web app

# AUTHORS
Decentrala

# COPYRIGHT
**AGPLv3+**: GNU AGPL version 3 <https://gnu.org/licenses/agpl.html>
This is *free* software: you are free to change and redistribute it.
There is **NO WARRANTY**, to the extent permitted by law.

